package controller

import (
	"encoding/json"
	"fmt"
	"strconv"

	"log"
	"net/http"

	"go-periksain-sample/models"

	_ "github.com/lib/pq"
)

type ResponseProperties struct {
	Page     uint `json:"page"`
	Total    uint `json:"total"`
	PageSize uint `json:"page_size"`
}

type Response struct {
	Success    bool                `json:"success"`
	Message    string              `json:"message"`
	Properties ResponseProperties  `json:"properties"`
	Payload    []models.CustomData `json:"payload"`
}

func AddCustom(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	page, errPage := strconv.Atoi(r.URL.Query().Get("page"))
	pageSize, errPageSize := strconv.Atoi(r.URL.Query().Get("page"))
	if errPage != nil {
		log.Fatalf("Failed to fetch. %v", errPage)
	}
	if errPageSize != nil {
		log.Fatalf("Failed to fetch. %v", errPageSize)
	}

	fetchResult := models.GetAllCustomData(page, pageSize)
	// if err != nil {
	// 	log.Fatalf("Failed to fetch. %v", err)
	// }

	fmt.Println(page)

	var defaultResponseProperties ResponseProperties
	defaultResponseProperties.Page = 2
	defaultResponseProperties.Total = 407
	defaultResponseProperties.PageSize = 3

	var response Response
	response.Success = true
	response.Message = "tambah spesialis berhasil"
	response.Properties = defaultResponseProperties
	response.Payload = fetchResult

	json.NewEncoder(w).Encode(response)
}
