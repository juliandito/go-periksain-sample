package router

import (
	"go-periksain-sample/controller"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {

	router := mux.NewRouter()

	router.HandleFunc("/api/custom-endpoint", controller.AddCustom).Methods("POST", "OPTIONS").Queries("page", "{page:[0-9]+}", "page_size", "{page_size:[0-9]+}")

	return router
}
