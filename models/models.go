package models

import (
	// "go-periksain-sample/config"
	// "log"

	_ "github.com/lib/pq"
)

type Registrasi struct {
	ID uint64 `json:"id"`
}
type Pasien struct {
	Nama string `json:"nama"`
}
type DokterPengirim struct {
	Nama string `json:"nama"`
}
type UnitAsal struct {
	Nama string `json:"nama"`
}
type Penjamin struct {
	Nama string `json:"nama"`
}
type PaketPemeriksaan struct {
	Nama string `json:"nama"`
}

type CustomData struct {
	Registrasi       Registrasi         `json:"registrasi"`
	Pasien           Pasien             `json:"pasien"`
	DokterPengirim   DokterPengirim     `json:"dokter_pengirim"`
	UnitAsal         UnitAsal           `json:"unit_asal"`
	Penjamin         Penjamin           `json:"penjamin"`
	PaketPemeriksaan []PaketPemeriksaan `json:"paket_pemeriksaan"`
}

func GetAllCustomData(page int, pageSize int) []CustomData {
	// db := config.CreateConnection()
	// defer db.Close()

	// offset := (page - 1) * pageSize
	// sqlStatement := `SELECT * FROM t_registrasi LIMIT $1 OFFSET $2`
	// rows, err := db.Query(sqlStatement, page, offset)
	// if err != nil {
	// 	log.Fatalf("Failed to execute query. %v", err)
	// }

	// defer rows.Close()

	var result []CustomData

	// for rows.Next() {
	// 	var rowValue CustomData
	// 	err = rows.Scan(&rowValue.ID, &rowValue.Judul_buku, &rowValue.Penulis, &rowValue.Tgl_publikasi)

	// 	if err != nil {
	// 		log.Fatalf("Failed to execute query. %v", err)
	// 	}
	// 	result = append(result, rowValue)
	// }

	var newRegistrasi Registrasi
	newRegistrasi.ID = 1

	var newPasien Pasien
	newPasien.Nama = "name"

	var newDokterPengirim DokterPengirim
	newDokterPengirim.Nama = "name"

	var newUnitAsal UnitAsal
	newUnitAsal.Nama = "name"

	var newPenjamin Penjamin
	newPenjamin.Nama = "name"

	var tryCustomData CustomData
	tryCustomData.Registrasi = newRegistrasi
	tryCustomData.Pasien = newPasien
	tryCustomData.DokterPengirim = newDokterPengirim
	tryCustomData.UnitAsal = newUnitAsal
	tryCustomData.Penjamin = newPenjamin

	result = append(result, tryCustomData)

	return result
}
