-- Table: public.m_kategori_pemeriksaan

-- DROP TABLE IF EXISTS public.m_kategori_pemeriksaan;

CREATE TABLE IF NOT EXISTS public.m_kategori_pemeriksaan
(
    id integer NOT NULL,
    nama character varying(45) COLLATE pg_catalog."default",
    no_urut integer,
    kode character varying(45) COLLATE pg_catalog."default",
    updated timestamp without time zone,
    created timestamp without time zone,
    CONSTRAINT m_kategori_pemeriksaan_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.m_kategori_pemeriksaan
    OWNER to postgres;