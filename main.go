package main

import (
	"fmt"
	"go-periksain-sample/router"
	"log"
	"net/http"
)

func main() {
	r := router.Router()
	fmt.Println("Server started at port 8080...")

	log.Fatal(http.ListenAndServe(":8080", r))
}
