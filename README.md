# Simple Periksain ID Sample API

built with golang and postgresql

## Installation
```
docker-compose up
```
- Create db server
- Connect server
- Make sure database server is running, and database "periksain_sample_db" exist 
- Adjust env if needed
- Start golang server
```
go run main.go
```
